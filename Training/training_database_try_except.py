"""

Обработка исключений на примере метода execute при возникновении ошибки.

"""

import sqlite3

con = sqlite3.connect('sw_inventory2.db')
query = "INSERT into switch values ('0000.AAAA.DDDD', 'sw7', 'Cisco 2960', 'London, Green Str')"

try:
    con.execute(query)
except sqlite3.IntegrityError as e:
    print("Error occured: ", e)