"""

Cursor как итератор
Если нужно построчно обрабатывать результирующие строки, лучше использовать курсор как итератор.
При этом не нужно использовать методы fetch.
При использовании методов execute возвращается курсор. А, так как курсор можно использовать как итератор,
можно использовать его, например, в цикле for.

"""

import sqlite3

connection = sqlite3.connect('sw_inventory.db')
cursor = connection.cursor()
result = cursor.execute('select * from switch')
for row in result:
    print(row)

"Аналогично без присваивания переменной"

print('Без переменной:')
for row in cursor.execute('select * from switch'):
    print(row)
