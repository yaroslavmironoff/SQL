"""

Создание файла БД, плдключение к БД и создание таблицы

"""


import sqlite3

conn = sqlite3.connect('dhcp_snooping.db')

print("Creating scheme...")
with open('dhcp_snooping_schema.sql', 'r') as f:
    schema = f.read()
    conn.executescript(schema)
print("Done")

conn.close()


"""

при выполнении строки conn = sqlite3.connect('dhcp_snooping.db'):
    создается файл dhcp_snooping.db, если его нет
    создается объект Connection

в БД создается таблица (если ее не было) на основании команд, которые указаны в файле 
dhcp_snooping_schema.sql:
    открывается файл dhcp_snooping_schema.sql
    schema = f.read() - весь файл считывается в одну строку
    conn.executescript(schema) - метод executescript позволяет выполнять команды SQL, которые прописаны
    в файле

"""