"""

Получение результатов запроса

Метод fetchone

Метод fetchone возвращает одну строку данных. Пример получения информации из базы данных
sw_inventory.db
"""

import sqlite3

connection = sqlite3.connect('sw_inventory.db')
cursor = connection.cursor()
print(cursor.execute('select * from switch'))
print(cursor.fetchone())

"""

Запрос SQL подразумевает, что запрашивалось всё содержимое таблицы, метод fetchone вернул
только одну строку.
Если повторно вызвать метод, он вернет следующую строку:

"""

print(cursor.fetchone())

"""

После обработки всех строк метод начинает возвращать None. За счет этого метод можно 
использовать в цикле

"""

print(cursor.execute('select * from switch'))
while True:
    next_row = cursor.fetchone()
    if next_row:
        print(next_row)
    else:
        break

"Метод fetchmany для возвращения списка строк данных"

cursor.execute('select * from switch')

from pprint import pprint

while True:
    three_rows = cursor.fetchmany(3)
    if three_rows:
        pprint(three_rows)
    else:
        break

"Метод fetchall для возвращения всех строк в виде списка"

cursor.execute('select * from switch')
print(cursor.fetchall())

"""

Если до метода fetchall использовался, например, метод fetchone, то метод fetchall вернет
оставшиеся строки запроса

"""

cursor.execute('select * from switch')
cursor.fetchone()
cursor.fetchone()
print(cursor.fetchall())